package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;


import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    StackPane[]         set;
    StackPane[]         alphabetBoxes;
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;
    HBox remainingGuessBox;

    HBox guessedLetters;
    HBox alphabetLineBox1;
    HBox alphabetLineBox2;
    HBox alphabetLineBox3;
    HBox alphabetLineBox4;
    char[] alphabetArray;


    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
        }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {
        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        discovered = 0;

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        gamedata.init();
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        remainingGuessBox = gameWorkspace.getRemainingGuessBox();

        guessedLetters    = (HBox)gameWorkspace.getGameTextsPane().getChildren().get(1);
        alphabetLineBox1 = (HBox)gameWorkspace.getGameTextsPane().getChildren().get(2);
        alphabetLineBox2 = (HBox)gameWorkspace.getGameTextsPane().getChildren().get(3);
        alphabetLineBox3 = (HBox)gameWorkspace.getGameTextsPane().getChildren().get(4);
        alphabetLineBox4 = (HBox)gameWorkspace.getGameTextsPane().getChildren().get(5);

        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters);

        play();
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            if (!success)
                endMessage += String.format(" (the word was \"%s\")", gamedata.getTargetWord());
            if (dialog.isShowing())
                dialog.toFront();
            else
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });
    }

    private void initWordGraphics(HBox guessedLetters)
    {

        char[] targetword = gamedata.getTargetWord().toCharArray();//original
        int difficulty = getDifficulty(targetword);//return the difficulty of the game.
        if(difficulty>=7){
            setHintButton(true);}

        progress = new Text[targetword.length];//original
        for (int i = 0; i < progress.length; i++) {//original
            progress[i] = new Text(Character.toString(targetword[i]));//original

            progress[i].setVisible(false);//original
            }

            addAlphabet();


    }


    public void setAlphabetBoxes(){
        alphabetArray = gamedata.getAlphabet().toCharArray();
        alphabetBoxes = new StackPane[26];//created
        //creates 26 boxes for the alphabet
        for(int i =0; i<26;i++){
            alphabetBoxes[i] = new StackPane();
            alphabetBoxes[i].setPrefSize(34,34);
            alphabetBoxes[i].setStyle("-fx-background-color: aliceblue");
            alphabetBoxes[i].getChildren().add(new Text(Character.toString(alphabetArray[i])));}
    }


    public void addAlphabet() {
        set = new StackPane[progress.length];//created

        setAlphabetBoxes();

        for (int i = 0; i < progress.length; i++) {//original
            set[i] = new StackPane();
            set[i].setPrefSize(20, 20);
            set[i].setStyle("-fx-background-color: aliceblue");
            set[i].getChildren().add(progress[i]);}

        guessedLetters.getChildren().addAll(set);

        alphabetLineBox1.getChildren().addAll(copy(alphabetBoxes,0,7));
        alphabetLineBox2.getChildren().addAll(copy(alphabetBoxes,7,14));
        alphabetLineBox3.getChildren().addAll(copy(alphabetBoxes,14,21));
        alphabetLineBox4.getChildren().addAll(copy(alphabetBoxes,21,26));

    }
    /**Helper method to copy array */
    private StackPane[] copy(StackPane[] arr,int start, int end){
        StackPane[] newArr = new StackPane[end-start];
        System.arraycopy(arr,start,newArr,0,end-start);
        return newArr;}

    private void guessHint(Text[] progress){
        ArrayList<Text> hint = new ArrayList<Text>();
        int random = 0, randomIndex = 0;

        for(int i =0;i<progress.length;i++){
            if(!(progress[i].isVisible())){
                hint.add(progress[i]);}}

        random = hint.size();
        randomIndex = (int)(Math.random()*random);

        for(int i = 0;i < progress.length;i++){

            if ((progress[i].getText()).equals(hint.get(randomIndex).getText())){
                progress[i].setVisible(true);
                gamedata.addGoodGuess(Character.toUpperCase(hint.get(randomIndex).getText().charAt(0)));
                discovered++;
            }
        }

        setHintButton(false);
        char c = hint.get(randomIndex).getText().charAt(0);
        int index=(((int)c)-97)>=0?(((int)c)-97):(((int)c)-65);
        alphabetBoxes[index].setStyle("-fx-background-color:limegreen");
        }


    public void setHangmanProgress(){

        boolean hintUsed = false;

        int dificulty = getDifficulty(gamedata.getTargetWord().toCharArray());

        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        int iter = 10-gamedata.getShowHangMan();
        for(int i = 0;i<iter;i++){
            gameworkspace.shapeSetVisible(i);}



            for(int i =0;i<alphabetBoxes.length;i++) {


                if (gamedata.getBadGuesses().contains(Character.toLowerCase(alphabetArray[i])))
                    alphabetBoxes[i].setStyle("-fx-background-color:orangered");

                if (gamedata.getGoodGuesses().contains(Character.toLowerCase(alphabetArray[i])))
                    alphabetBoxes[i].setStyle("-fx-background-color:limegreen");


                if (gamedata.getGoodGuesses().contains(Character.toUpperCase(alphabetArray[i]))) {
                    alphabetBoxes[i].setStyle("-fx-background-color:limegreen");
                    hintUsed = true;

                    }
                }
                if((dificulty>=7)&&(!hintUsed)){
                setHintButton(true);
                }

    }




    private int getDifficulty(char[] arr) {
        if (arr.length > 0) {
            int counter = 1;
            char[] newArray = new char[arr.length];
            for (int i = 0; i < arr.length; i++) {
                newArray[i] = arr[i];
            }
            Arrays.sort(newArray);

            char init = newArray[0];
            for (int i = 0; i < arr.length; i++) {
                if (newArray[i] > init) {
                    counter++;
                    init = newArray[i];}
                 }
            return counter;
        }
        return 0;

        }

    public void play() {

        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {

                ((HBox)((Workspace)appTemplate.getWorkspaceComponent()).getGameTextsPane().
                        getChildren().get(6)).getChildren().get(0).setOnMouseClicked(e->{
                    guessHint(progress);
                    gamedata.decreaseRemainingGuesses();
                    success = (discovered == progress.length);
                    remains.setText(Integer.toString(gamedata.getRemainingGuesses()));

                        });

                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);
                    int indexAlphabet = (((int)guess)-97)>=0?(((int)guess)-97):(((int)guess)-65);
                    if(String.valueOf(guess).matches("[a-zA-Z]*")){
                        if (!alreadyGuessed(guess)) {
                            boolean goodguess = false;
                            for (int i = 0; i < progress.length; i++) {
                                if (gamedata.getTargetWord().charAt(i) == guess) {
                                    progress[i].setVisible(true);
                                    gamedata.addGoodGuess(guess);
                                    goodguess = true;
                                    discovered++;
                                    //Colors the alphabet table
                                    alphabetBoxes[indexAlphabet].setStyle("-fx-background-color:limegreen");

                                }
                            }
                            if (!goodguess){
                                gameworkspace.shapeSetVisible(10-gamedata.getShowHangMan());
                                gamedata.addBadGuess(guess);

                                alphabetBoxes[indexAlphabet].setStyle("-fx-background-color:orangered");


                            }

                            success = (discovered == progress.length);
                            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));


                        }
                        setGameState(GameState.INITIALIZED_MODIFIED);
                    }
                });
                if (gamedata.getRemainingGuesses() <= 0 || success){
                    gameworkspace.shapeSetVisible(9);
                    stop();
                }
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    //Visibility of hint button
    private void setHintButton(boolean visibility){
        ((HBox)((Workspace)appTemplate.getWorkspaceComponent()).getGameTextsPane().
                getChildren().get(6)).getChildren().get(0).setVisible(visibility);
    }

    private void restoreGUI() {
        disableGameButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        guessedLetters   = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        HBox alphabetLineBox1    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(2);
        HBox alphabetLineBox2    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(3);
        HBox alphabetLineBox3    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(4);
        HBox alphabetLineBox4    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(5);

        setAlphabetBoxes();
        setHangmanProgress();

        alphabetLineBox1.getChildren().addAll(copy(alphabetBoxes,0,7));
        alphabetLineBox2.getChildren().addAll(copy(alphabetBoxes,7,14));
        alphabetLineBox3.getChildren().addAll(copy(alphabetBoxes,14,21));
        alphabetLineBox4.getChildren().addAll(copy(alphabetBoxes,21,26));



        restoreWordGraphics(guessedLetters);

        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);

        success = false;

        play();
    }

    private void restoreWordGraphics(HBox guessedLetters) {
        discovered = 0;
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        set = new StackPane[targetword.length];//created

        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            set[i] = new StackPane(); // added.
            set[i].setPrefSize(20, 20);
            set[i].setStyle("-fx-background-color: aliceblue");
            set[i].getChildren().add(progress[i]);

            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            if (progress[i].isVisible())
                discovered++;
        }
        guessedLetters.getChildren().addAll(set);
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {

        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {

        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());

            File f = targetPath.toFile();
            if(!f.exists())
                if(!f.mkdir())
                    f.mkdirs();

            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());


            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {



        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED)){

            load = promptToSave();}
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);

            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists()){
                load(selectedFile.toPath());
            restoreGUI(); }// restores the GUI to reflect the state in which the loaded game was last saved



        }

    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {

            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                               propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {

        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }

    public void print(String s){
        System.out.println(s);
    }
}

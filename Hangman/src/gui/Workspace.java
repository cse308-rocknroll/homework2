package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;
import javafx.scene.shape.Line;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;
import java.util.ArrayList;

import static hangman.HangmanProperties.*;

//For shape

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    BorderPane         figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    Button            startGame;         // the button to start playing a game of Hangman
    Button            hintButton;
    HangmanController controller;
    HBox              alphabetLineBox1;
    HBox              alphabetLineBox2;
    HBox              alphabetLineBox3;
    HBox              alphabetLineBox4;
    HBox              hintButtonBox;
    Circle            head;
    ArrayList<Shape> shapeSet = new ArrayList<>();


    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));


        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);


        figurePane = new BorderPane();
        guessedLetters = new HBox(5);

        //hangman drawing
        Circle circle = new Circle(30);
        circle.setFill(Color.BLUE);


        /**
        alphabetLineBox1 = new HBox(1);
        alphabetLineBox2 = new HBox(1);
        alphabetLineBox3 = new HBox(1);
        alphabetLineBox4 = new HBox(1);
        hintButtonBox    = new HBox(1);

        guessedLetters.setStyle("-fx-background-color: transparent;");
        guessedLetters.setStyle("-fx-background-color: transparent;");
        alphabetLineBox1.setStyle("-fx-background-color: transparent;");
        alphabetLineBox2.setStyle("-fx-background-color: transparent;");
        alphabetLineBox3.setStyle("-fx-background-color: transparent;");
        alphabetLineBox4.setStyle("-fx-background-color: transparent;");
         */

        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();

        //gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, alphabetLineBox1, alphabetLineBox2, alphabetLineBox3, alphabetLineBox4);

        bodyPane = new HBox();

        // Modified code

        figurePane.setStyle("-fx-background-color: lightskyblue;");
        figurePane.setPrefHeight(500);
        figurePane.setPrefWidth(900);
       // Button test = new Button("just to find figure pane");
        circle.centerXProperty().bind(figurePane.widthProperty().divide(2));
        circle.centerYProperty().bind(figurePane.heightProperty().divide(4));

        linePane(figurePane,4,1.1,2,1.1);//4 base platform
        linePane(figurePane,4,16,4,1.1);//3 vLine
        linePane(figurePane,4,16,2,16);//2 hLine
        linePane(figurePane,2,16,2,4);//1 hanging line



        shapeSet.add(circle);

        linePane(figurePane,2,3.3,2,2.2);//spinal

        linePane(figurePane,2,2.2,1.8,1.8);//left leg
        linePane(figurePane,2,2.2,2.2,1.8);//right leg

        linePane(figurePane,2,2.8,1.8,2.4);//left arm
        linePane(figurePane,2,2.8,2.2,2.4);//right right
        figurePane.getChildren().add(circle);//

        for(int i =0; i<shapeSet.size();i++){
            shapeSet.get(i).setVisible(false);
        }



        bodyPane.getChildren().addAll(figurePane, gameTextsPane);

        startGame = new Button("Start Playing");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);

        workspace = new VBox();
        workspace.getChildren().addAll(headPane, bodyPane, footToolbar);
    }

    public void linePane(BorderPane figurePane, double x1, double y1, double x2, double y2) {

        Line line = new Line();
        line.startXProperty().bind(figurePane.widthProperty().divide(x1));
        line.startYProperty().bind(figurePane.heightProperty().divide(y1));
        line.endXProperty().bind(figurePane.widthProperty().divide((x2)));
        line.endYProperty().bind(figurePane.heightProperty().divide(y2));
        line.setStrokeWidth(5);
        line.setStroke(Color.BLACK);
        figurePane.getChildren().add(line);
        shapeSet.add(line);
    }

    public void shapeSetVisible(int i){
        shapeSet.get(i).setVisible(true);
        }
    public void shapeDisable(int i){
        shapeSet.get(i).setVisible(false);
    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public HBox getAlphabetLineBox1() {
        return alphabetLineBox1;
    }
    public HBox getAlphabetLineBox2() { return alphabetLineBox2; }
    public HBox getAlphabetLineBox3() { return alphabetLineBox3; }
    public HBox getAlphabetLineBox4() { return alphabetLineBox4; }

    public Button getStartGame() {
        return startGame;
    }
    public int getShapeSetSize(){return shapeSet.size();}

    public void reinitialize() {

        for(int i=0;i<getShapeSetSize();i++){shapeDisable(i);}

        guessedLetters = new HBox(5);
        guessedLetters.setPadding(new Insets(1,1,1,1));
        alphabetLineBox1 = new HBox(1);
        alphabetLineBox1.setPadding(new Insets(100,1,1,1));
        alphabetLineBox2 = new HBox(1);
        alphabetLineBox2.setPadding(new Insets(1,1,1,1));
        alphabetLineBox3 = new HBox(1);
        alphabetLineBox3.setPadding(new Insets(1,1,1,1));
        alphabetLineBox4 = new HBox(1);
        alphabetLineBox4.setPadding(new Insets(1,1,1,1));
        hintButtonBox = new HBox();
        hintButton = new Button("HINT");
        hintButton.setVisible(false);
        hintButtonBox.setPadding(new Insets(20,1,0,0));

        hintButtonBox.getChildren().add(hintButton);

        guessedLetters.setStyle("-fx-background-color: transparent;");
        alphabetLineBox1.setStyle("-fx-background-color: transparent;");
        alphabetLineBox2.setStyle("-fx-background-color: transparent;");
        alphabetLineBox3.setStyle("-fx-background-color: transparent;");
        alphabetLineBox4.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();

        remainingGuessBox.setPadding(new Insets(1,1,1,1));


        gameTextsPane = new VBox();

        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters,alphabetLineBox1, alphabetLineBox2, alphabetLineBox3, alphabetLineBox4,hintButtonBox);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);
    }

}
